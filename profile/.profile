export ANDROID_HOME="$HOME/Android/Sdk"
export PATH="$ANDROID_HOME/platform-tools:$ANDROID_HOME/emulator:$ANDROID_HOME/tools:$HOME/.local/bin:$HOME/.cargo/bin:$HOME/node_modules/.bin:$HOME/.gem/ruby/2.6.0/bin:$HOME/.luarocks/bin:$PATH"
export EDITOR="emacsclient -ca ''"
